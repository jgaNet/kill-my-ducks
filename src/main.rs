//GOAL : cargo run searchstring example-filename.txt

use std::env;
use std::process;
use kill_my_ducks::*;

fn main() {
    let args: Vec<String> = env::args().collect();
    let config = match Config::new(&args) {
        Ok(config) => config,
        Err(e) => {
            eprintln!("Duck config error: {}", e);
            process::exit(1);
        }
    };

    if let Err(e) = kill_my_ducks::run(config) {
        eprintln!("Duck error: {}", e);
        process::exit(1);
    };
}
extern crate rand;

use rand::Rng;
use std::collections::hash_map::DefaultHasher;
use std::error::Error;
use std::fs::{self, File};
use std::hash::{Hash, Hasher};
use std::io;
use std::io::prelude::*;
use std::path::{Path, PathBuf};

const TMP_FOLDER: &'static str = "./tmp";
const DUCK_PATTERN: &'static str = "@duck";
const DUCKS_IGNORE_FILENAME: &'static str = ".ducksignore";

pub struct Config {
    query: String,
    root_path: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &'static str> {
        if args.len() < 2 {
            return Err("Missing arguments");
        }

        Ok(Config {
            query: String::from(DUCK_PATTERN),
            root_path: args[1].clone(),
        })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let files_to_search_duck_content =
        kill_ducks_in_files_system(&config.query, Path::new(&config.root_path))?;
    create_tmp_folder()?;
    kill_ducks_in_files_content(&config.query, &files_to_search_duck_content)?;
    remove_tmp_folder()?;

    println!("Hunt is finished!");

    Ok(())
}

fn create_tmp_folder() -> std::io::Result<()> {
    if Path::new(TMP_FOLDER).is_dir() {
        remove_tmp_folder()?;
    }
    fs::create_dir(TMP_FOLDER)?;
    Ok(())
}

fn remove_tmp_folder() -> std::io::Result<()> {
    fs::remove_dir_all(TMP_FOLDER)?;
    Ok(())
}

fn kill_ducks_in_files_system<'a>(duck_pattern: &str, dir: &'a Path) -> io::Result<Vec<PathBuf>> {
    let mut files_to_search_duck_content: Vec<PathBuf> = Vec::new();
    if dir.is_dir() {
        if !find_dockignore(dir.clone()).unwrap() {
            for entry in fs::read_dir(dir)? {
                println!("------- we're hunting the duck... -------");
                let path = entry?.path();
                let pathname = path.to_str().unwrap();
                if path.is_dir() {
                    kill_duck_folder(
                        duck_pattern,
                        path.clone(),
                        pathname,
                        &mut files_to_search_duck_content,
                    )?;
                } else {
                    kill_duck_file(
                        duck_pattern,
                        path.clone(),
                        pathname,
                        &mut files_to_search_duck_content,
                    )?;
                }
            }
        } else {
            println!("A {} file found. Hunt is forbidden in {:?}", DUCKS_IGNORE_FILENAME, dir.clone());
        }
    }

    Ok(files_to_search_duck_content)
}

fn find_dockignore(dir: &Path)  -> io::Result<bool> {
    let mut duckignore_found = false;
    for entry in fs::read_dir(dir)? {
        let path = entry?.path();
        let filename = path.file_name().unwrap();
        if !path.is_dir() && filename == DUCKS_IGNORE_FILENAME {
            duckignore_found = true;
            break;
        }
    }

    Ok(duckignore_found)
}

fn kill_duck_folder(
    duck_pattern: &str,
    path: PathBuf,
    pathname: &str,
    files_to_search_duck_content: &mut Vec<PathBuf>,
) -> io::Result<()> {
    if pathname.contains(duck_pattern) {
        println!("------- Duck <folder> found -------");
        println!("path: {}", pathname);
        println!(
            "Would you kill this duck <folder> ?"
        );
        if ask_user_action(false).unwrap() {
            println!("------- Duck <folder> killed -------");
            fs::remove_dir_all(pathname)?;
        } else {
            println!("------- Duck <folder> alive -------");
        };
        println!("");
    } else {
        let mut child_files = kill_ducks_in_files_system(duck_pattern, &path)?;
        files_to_search_duck_content.append(&mut child_files);
    }

    Ok(())
}

fn kill_duck_file(
    duck_pattern: &str,
    path: PathBuf,
    pathname: &str,
    files_to_search_duck_content: &mut Vec<PathBuf>,
) -> io::Result<()> {
    if pathname.contains(duck_pattern) {
        println!("------- Duck <file> found -------");
        println!("path: {}", pathname);
        println!(
            "Would you kill this duck <file> ?"
        );
        if ask_user_action(false).unwrap() {
            println!("------- Duck <file> killed -------");
            fs::remove_file(pathname)?;
        } else {
            println!("------- Duck <file> alive -------");
        };
        println!("");
    } else {
        files_to_search_duck_content.push(path.clone());
    }

    Ok(())
}

fn kill_ducks_in_files_content(duck: &str, files: &Vec<PathBuf>) -> Result<(), Box<dyn Error>> {
    for file in files {
        let mut line_number = 0;
        let mut at_least_one_duck = false;
        let (tmp_buffer_file_path, mut tmp_buffer_file) = create_tmp_file().unwrap();
        let content = match fs::read_to_string(file) {
            Ok(content) => content,
            Err(e) => format!("{}", e),
        };

        for line in content.lines() {
            line_number += 1;
            if line.contains(duck) {
                at_least_one_duck = true;
                println!("------- Duck <content> found -------");
                println!("    file : {}", String::from(file.to_str().unwrap()));
                println!("    line : {}", line_number);
                println!("    content : {}", String::from(line));
                println!("Would you kill this duck <content> ?");
                if ask_user_action(false).unwrap() {
                    println!("------- Duck <content> killed -------");
                    println!("");
                    continue;
                } else {
                    println!("------- Duck <content> alive -------");  
                    println!("");     
                };
                
            }
            if let Err(e) = writeln!(tmp_buffer_file, "{}", line) {
                eprintln!("Couldn't write to file: {}", e);
            }
        }

        drop(tmp_buffer_file);
        drop(file);

        if at_least_one_duck {
            fs::remove_file(file)?;
            fs::copy(tmp_buffer_file_path.clone(), file)?;
        }

        fs::remove_file(tmp_buffer_file_path)?;
    }
    Ok(())
}

fn create_tmp_file() -> Result<(PathBuf, std::fs::File), Box<dyn Error>> {
    let mut hasher = DefaultHasher::new();
    let numbers = rand::thread_rng().gen_range(0, 100000000);
    Hash::hash(&numbers, &mut hasher);
    let tmp_buffer_file_path = Path::new(TMP_FOLDER).join(format!("{:x}", hasher.finish()));
    let tmp_buffer_file = File::create(tmp_buffer_file_path.clone())?;

    Ok((tmp_buffer_file_path, tmp_buffer_file))
}

fn ask_user_action(try_again: bool) -> Result<bool, Box<dyn Error>> {
    let mut input = String::new();
    if try_again {
        println!("Sorry, I didn't understand. Please try again.");
    }

    println!("Press <Enter> for trigger (default : Y(es)) or write n(o) to let it alive");
    io::stdin().read_line(&mut input)?;
    
    match input.as_str() {
        "n\n" => Ok(false),
        "no\n" => Ok(false),
        "Y\n" =>  Ok(true),
        "Yes\n" =>  Ok(true),
        "\n" =>  Ok(true),
        _ => ask_user_action(true),
    }
}
